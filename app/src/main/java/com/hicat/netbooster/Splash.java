package com.hicat.netbooster;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Splash extends AppCompatActivity {

    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=this;
        final AlertDialog dialog= new AlertDialog.Builder(this).create();
        dialog.setTitle(getString(R.string.error));
        dialog.setMessage(getString(R.string.dialog_text_error));
        dialog.setButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//
//                if (!isNetworkAvailable())
//                    dialog.show();
//                else {
                     startActivity(new Intent(getApplicationContext(), com.hicat.netbooster.MainActivity.class));
                    com.hicat.netbooster.SpotTheCatApp.showInterstitial();
                finish();
//                }
            }
        },3000L);
    }

    public boolean isNetworkAvailable() {
        boolean var3 = false;
        ConnectivityManager var4 = (ConnectivityManager)this.getSystemService("connectivity");
        boolean var2;
        if(var4 == null) {
            var2 = var3;
        } else {
            NetworkInfo[] var5 = var4.getAllNetworkInfo();
            var2 = var3;
            if(var5 != null) {
                int var1 = 0;

                while(true) {
                    var2 = var3;
                    if(var1 >= var5.length) {
                        break;
                    }

                    if(var5[var1].getState() == NetworkInfo.State.CONNECTED) {
                        var2 = true;
                        break;
                    }

                    ++var1;
                }
            }
        }
       // Toast.makeText(this,Boolean.valueOf(var2)+"",Toast.LENGTH_SHORT).show();
        return var2;
    }


}
