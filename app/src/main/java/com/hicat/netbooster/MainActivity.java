// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.hicat.netbooster;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Iterator;

import butterknife.ButterKnife;
import butterknife.InjectView;

@SuppressWarnings("ResourceType")
public class MainActivity extends AppCompatActivity
{


    private NotificationManager mNotificationManager;

    private SharedPreferences prefs;
    private boolean showAd;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.progress_view)
    CircularProgressView circularProgressView;
    @InjectView(R.id.imageView3)
    ImageView imageView;
    @InjectView(R.id.textView3)
    TextView textView;
    @InjectView(R.id.button1)
            TextView progres;
    @InjectView(R.id.frame)
            View frame;
    @InjectView(R.id.imageButton)
            View no_play;
    @InjectView(R.id.button)
            View button;
    @InjectView(R.id.memory)
            TextView memory;
    Kill kill;
    private AdView adView;
    private AdRequest var13;
    AnimatorSet animatorSet;

    int total_proccess=0;
    int initial_memory=0;
    public MainActivity()
    {

    }

    private void cancelNotification()
    {
        mNotificationManager.cancel(1);
    }

    private boolean haveInternet(Context context)
    {
        NetworkInfo cmng = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (cmng == null || !cmng.isConnected())
        {
            return false;
        }
        return !cmng.isRoaming();
    }

    private void killApps() {
       kill.execute();
    }

    private void showNotification()
    {
//        Notification notification = new Notification(0x7f02001f, "Internet Connection Boost Activated!", System.currentTimeMillis());
//        notification.setLatestEventInfo(getApplicationContext(), "Internet Connection Boost Activated!", "", PendingIntent.getActivity(this, 0, new Intent(this, com/mandy/internetspeedboosterfree/MainActivity), 0));
//        notification.flags = notification.flags | 0x22;
//        mNotificationManager.notify(1, notification);
    }

    public void handleBoost(View view)
    {
        SharedPreferences pf=getSharedPreferences("app",MODE_PRIVATE);
        view.setVisibility(View.GONE);
        progres.setVisibility(View.VISIBLE);
       // if(!pf.getBoolean("o",false)){
            killApps();
            pf.edit().putBoolean("o",true).commit();
       // }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    public void onBackPressed()
    {
      finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(com.hicat.netbooster.R.layout.activity_main);
        ButterKnife.inject(this);

//        SpannableString tittle= new SpannableString(getString(R.string.splash_text1)+" "+getString(R.string.splash_text2));
//        tittle.setSpan(new CustomTypeFaceSpan("", SpotTheCatApp.faceR), 0, tittle.length(),
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.parseColor("#834655"));
//        toolbar.setSubtitle(R.string.splash_text2);
        //toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_launcherr));
        setSupportActionBar(toolbar);

        button = (ImageButton)findViewById(com.hicat.netbooster.R.id.button);
        mNotificationManager = (NotificationManager)getSystemService("notification");
        prefs = getSharedPreferences("data", 0);
          if (prefs.getBoolean("isOn", false))
        {
            //button.setText("Stop Boost");
        }
        circularProgressView.setProgress(0.0f);
        kill =new Kill(this);
        this.adView = (AdView)this.findViewById(com.hicat.netbooster.R.id.adView);
        var13 = (new AdRequest.Builder()).build();
        this.adView.loadAd(var13);
        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        });
        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(),
                R.anim.flip);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       SpotTheCatApp.showInterstitial();
                   }
               }, 1500);
            }
        });
    }
    private long memoryInfo() {
        ActivityManager.MemoryInfo var1 = new ActivityManager.MemoryInfo();
        ((ActivityManager)this.getSystemService("activity")).getMemoryInfo(var1);
        long var2 = var1.availMem / 1048576L;
        return var2;
    }

    private int getProccess() {
        ActivityManager var2 = (ActivityManager)this.getSystemService("activity");
        int var4 = 0;
        Iterator var5 = var2.getRunningAppProcesses().iterator();
        while(true) {
            ActivityManager.RunningAppProcessInfo var3;
            do {
                if(!var5.hasNext()) {
                    return var4;
                }
                var3 = (ActivityManager.RunningAppProcessInfo)var5.next();
            } while(var3.processName.equals("system_process") && var3.processName.equals(this.getPackageName()));
            ++var4;

        }

    }
    class Kill extends AsyncTask<Void,Float,Void>{

        private Context context ;
        private ActivityManager mActivityManager;
        private float total;
        public Kill(Context context) {
            super();
            this.context=context;
            mActivityManager = (ActivityManager) getSystemService("activity");
            total=getPackageManager().getInstalledApplications(0).size()*1f;
            circularProgressView.setMaxProgress(total);
         }

        @Override
        protected void onPostExecute(Void aVoid) {
            SharedPreferences pf=getSharedPreferences("app",MODE_PRIVATE);
            pf.edit().putBoolean("o",false).commit();
            button.setVisibility(View.INVISIBLE);
            circularProgressView.setProgress(total);
//            circularProgressView.setVisibility(View.INVISIBLE);
            progres.setText("100%");

            animatorSet.setTarget(imageView);
            imageView.setVisibility(View.VISIBLE);
            animatorSet.start();
            progres.setVisibility(View.INVISIBLE);
            textView.setText(getString(com.hicat.netbooster.R.string.home_text3));
            memory.setText(String.format(getString(R.string.memory),Math.abs((int)(initial_memory - memoryInfo()))+""));
            Log.d("free memory",(initial_memory - memoryInfo())+"mb");
            Log.d("total proccess",(total_proccess-getProccess())+"mb");
        }

        @Override
        protected void onProgressUpdate(final Float... values) {
                    circularProgressView.setProgress(values[0].intValue());
                    progres.setText(((int)values[0].intValue()*100/(int)total)+"%");

        }

        @Override
        protected void onPreExecute() {
            total_proccess= getProccess();
            initial_memory= (int) memoryInfo();
            Log.d("used memory",initial_memory+"mb");
            Log.d("total proccess",total_proccess+"mb");
            progres.setText("0%");
//            progres.setVisibility(View.INVISIBLE);
            circularProgressView.setVisibility(View.VISIBLE);
            //button.setVisibility(View.GONE);
            textView.setText(getString(R.string.home_text2));
           // frame.setVisibility(View.VISIBLE);
            //no_play.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            int x=0;
            for (ApplicationInfo packageInfo : getPackageManager().getInstalledApplications(0)) {
                if (!packageInfo.packageName.equals(getPackageName())) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mActivityManager.killBackgroundProcesses(packageInfo.packageName);
                    x++;
                    publishProgress(x*1f);
                }
            }
            return null;
        }
    }
}
